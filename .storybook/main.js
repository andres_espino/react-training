module.exports = {
  core: {
    builder: "webpack5",
  },
  stories: ["../src/**/*.stories.js"],
  addons: [
    "@storybook/addon-actions",
    "@storybook/addon-links",
    "@storybook/addon-storysource",
    "@storybook/addon-toolbars",
    "@storybook/addon-controls",
    "@storybook/addon-a11y",
    "@storybook/addon-backgrounds",
    "@storybook/addon-viewport",
  ],
  babel: (config) => {
    config.presets.push(require.resolve("@emotion/babel-preset-css-prop"));
    return config;
  },
  webpackFinal: async (config, { configType }) => {
    return config;
  },
};