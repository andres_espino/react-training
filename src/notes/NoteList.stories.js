import React from 'react';
import {action} from "@storybook/addon-actions"

import NoteList  from "./NoteList";

import notes from "../mocks/notes";


export default {
  title: 'Components/NoteList',
  component: NoteList,
};

export const Default = () => {
  return <NoteList notes={notes} onDelete={action("onDeleteItem")} />
};
