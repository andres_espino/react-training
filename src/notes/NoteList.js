import React from 'react';
import Note from "./Note";
import styled from '@emotion/styled';

const Ul = styled.ul`
  display: flex;
  flex-wrap: wrap;
`;

const Li = styled.li`
  flex-basis: 20%;
  margin: 4px;
`;

export default function NodeList({notes, onDelete}) {
  return (
    <Ul>
      {notes.map(({title, description, _id}) =>
        <Li key={_id}>
          <Note title={title} description={description} id={_id} onDelete={onDelete}/>
        </Li>
      )}
    </Ul>
  );
};
