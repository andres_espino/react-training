import React, {useState, useEffect} from 'react';
import NoteList from "./NoteList";

const NOTES_URL = '/api/notes';

export default function ConnectedNotesList() {
  const [notes, setNotes] = useState([]);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");

  useEffect(() => {
    async function loadNotes() {
      try {
        setLoading(true);
        const response = await fetch(NOTES_URL, { headers: { "content-type": "application/json" } });
        setLoading(false);
        if (response.ok) {
          setNotes(await response.json());
        } else {
          setError('Unable to retrieve notes from server');
        }
      } catch (e) {
        setLoading(false);
        setError('Check network connection');
      }
    }
    loadNotes();
  }, []);

  const onDelete = async (id) => {
    try {
      const response = await fetch(NOTES_URL + '/'+ id, {
        headers:
          { "content-type": "application/json" },
        method: "DELETE"

      });

      if (response.status === 204) {
         const newArray = notes.filter(item => {
           return id !== item._id;
         });

        setNotes(newArray);
      }

    } catch(e) {
      setError('Check network connection');
    }
  };

  if (loading) {
    return "Loading...";
  }

  return (
    <div>
      <NoteList notes={notes} onDelete={onDelete}/>
    </div>
  );
}
