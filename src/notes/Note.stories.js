import React from 'react';
import {action} from "@storybook/addon-actions"
import Note from './Note';


export default {
  title: 'Components/Note',
  component: Note,
  args: {
    title: 'Note title',
    description: 'Example of description',
    id: 0,
    onDelete: action("onDelete"),
  },
};

export const Default = ({title, description, id, onDelete}) => {
  return <Note title={title} description={description} id={id} onDelete={onDelete} />
};
