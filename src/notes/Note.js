import React from 'react';
import styled from '@emotion/styled';
// `
const Button = styled.button`
  padding: 10px;
  border: 1px blue solid;
  border-radius: 4px;
`;

export default function Note({title, description, id, onDelete}) {
  return (
    <div id={`note-${id}`}>
      <h1>{title}</h1>
      <p>{description}</p>
      <Button onClick={() => onDelete(id)} data-testid="note_button">Delete this note</Button>
    </div>
  );
}
