import React from 'react';
import {render, fireEvent} from "@testing-library/react";
import Note from '../Note';

describe('Note Component tests', () => {
  const MOCK_NOTE_DATA = {
    title: 'Title',
    description: 'description',
    id:'234',
    onDelete: jest.fn(),
  }

  function renderNote() {
    return render(<Note {...MOCK_NOTE_DATA}/>);
  }
  beforeEach(() => {
    //jest restore
  });

  it('should render the Note Component HTML', () => {
    const {getByText} = renderNote();
    expect(getByText(MOCK_NOTE_DATA.title)).toBeVisible();
    expect(getByText(MOCK_NOTE_DATA.description)).toBeVisible();
  });

  it('should call onDelete with the Note id', () => {
    const {getByText} = renderNote();
    fireEvent.click(getByText("Delete this note"));
    expect(MOCK_NOTE_DATA.onDelete).toHaveBeenCalledWith(MOCK_NOTE_DATA.id);
    expect(MOCK_NOTE_DATA.onDelete).toHaveBeenCalledTimes(1);
  });
});
