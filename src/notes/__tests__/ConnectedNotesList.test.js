import React from 'react';
import { render, fireEvent, waitFor, getAllByText, getByText } from "@testing-library/react";
import notes from "../../mocks/notes";
import ConnectedNotesList from '../ConnectedNotesList';

describe('Note Component tests', () => {
  function renderConnectedNoteList() {
    return render(<ConnectedNotesList/>);
  }

  it('User can delete a note', async () => {
    const {findAllByTestId, queryByText} = renderConnectedNoteList();

    await findAllByTestId("note_button").then(elements => {
      expect(elements[1]).toBeVisible();

      fireEvent.click(elements[1]);
    });

    return waitFor(() => {
      expect(queryByText(notes[1].title)).toBe(null)
    })
  });
});
