import React from 'react';
import {render, fireEvent} from "@testing-library/react";
import notes from "../../mocks/notes";
import NoteList from '../NoteList';

describe('Note Component tests', () => {
  const MOCK_NOTE_DATA = {
    title: 'Title',
    description: 'description',
    id:'234',
    onDelete: jest.fn(),
  }

  function renderNoteList() {
    const spy = jest.fn();
    const utils = render(<NoteList notes={notes} onDelete={spy}/>);
    return {
      ...utils,
      spy
    };
  }

  beforeEach(() => {
    //jest restore
  });

  it ('should render list of notes', () => {
    const {getAllByText} = renderNoteList();
    expect(getAllByText(/Note Title/)).toHaveLength(5);
    expect(getAllByText(/Note Description/)).toHaveLength(5);
  });

  it('should callonDelete', () => {
    const {getAllByTestId, spy} = renderNoteList()
    fireEvent.click(getAllByTestId("note_button")[1]);
    expect(spy).toHaveBeenCalledTimes(1);
    expect(spy).toHaveBeenCalledWith(notes[1]._id);
  });
});
