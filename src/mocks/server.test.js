import notes from "./notes";

describe("server tests", () => {
  it("should return notes", () => {
    return fetch("/api/notes").then((res) => res.json()).then(noteList => {
      expect(Array.isArray(noteList)).toBe(true);
      expect(noteList.length).toBe(5);
    });
  });
  it("should return a specific note", () => {
    return fetch("/api/notes/1").then((res) => res.json()).then(note => {
      expect(note).toEqual(notes[1]);
    });
  });
  it("should create new notes", () => {
    return fetch("/api/notes", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      }
    }).then((res) => res.json()).then(note => {
      expect(note).toEqual({
        id: "new note id",
        title: "new note title",
        description: "new note description",
      });
    });
  });
});
