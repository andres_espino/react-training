import {rest} from "msw";
import {setupServer} from "msw/node";
import notes from "./notes";

const notesHandlers =  [
  rest.get("/api/notes", (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json(notes)
    );
  }),

  rest.get("/api/notes/:id", (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json(notes[req.params.id])
    );
  }),
  rest.post("/api/notes", (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json({
        id: "new note id",
        title: "new note title",
        description: "new note description",
      })
    );
  }),
  rest.put("/api/notes/:id", (req, res, ctx) => {
    return res(
      ctx.status(200),
      ctx.json(notes[req.params.id])
    );
  }),
  rest.delete("/api/notes/:id", (req, res, ctx) => {
    return res(
      ctx.status(204)
    );
  })
];

export default setupServer(...notesHandlers);
