export default [
  { _id: "0", title: "Note Title 1", description: "Note Description 1" },
  { _id: "1", title: "Note Title 2", description: "Note Description 2" },
  { _id: "2", title: "Note Title 3", description: "Note Description 3" },
  { _id: "3", title: "Note Title 4", description: "Note Description 4" },
  { _id: "4", title: "Note Title 5", description: "Note Description 5" },
];
