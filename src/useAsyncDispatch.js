import {useReducer} from "react";

export default function useAsyncDispatch(reducer, initalArg, init) {
  const [state, dispatch] = useReducer(reducer, initalArg, init);

  const betterDispatch = (value) => {
    if (typeof value === "function") {
      value(dispatch);
    } else {
      dispatch(value);
    }
  }

  return [state, betterDispatch];
}
