import React from "react";
import ReactDOM from "react-dom";
import ConnectedNotesList from "./notes/ConnectedNotesList";

const theme = {
  colors: {
    primary: "blue",
  }
}

function App() {
  return (
      <ConnectedNotesList />
  );
}

ReactDOM.render(<App />, document.querySelector("#root"));
